/*
 * File: ReferenceIndex.java
 * Author: Force Chalrie
 * CreateTime: @2015.10
 * Copyright (C) 2017. OSChina.NET.All Rights Reserved.
 */

package svnserver.oschina;

/**
 * Created by forceoschina on 15-10-30.
 */
public enum ReferenceIndex {
    ReferenceBranch, ReferenceTrunk, ReferenceTags, ReferenceHEAD
}
