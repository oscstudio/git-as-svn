/**
 * This file is part of git-as-svn. It is subject to the license terms
 * in the LICENSE file found in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/gpl-2.0.html. No part of git-as-svn,
 * including this file, may be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file.
 */
package svnserver.server;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNException;
import svnserver.VersionInfo;
import svnserver.config.Config;
import svnserver.config.serializer.ConfigSerializer;
import svnserver.oschina.Parameters;
import svnserver.oschina.Resque;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Entry point.
 *
 * @author a.navrotskiy
 */
public class Main {
  @NotNull
  private static final Logger log = LoggerFactory.getLogger(SvnServer.class);
  private static boolean ParametersInitialize() {
    try {
      java.net.URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
      String path ;
      File file=new File("sserver.toml");
      if(file.exists()){
        path="sserver.toml";
      }else{
        path = java.net.URLDecoder.decode(url.getPath(), "utf-8");
        if (path.endsWith(".jar")) {
          path = path.substring(0, path.lastIndexOf('/') + 1) + "/sserver.toml";
          //BrowseRecord.Instance().setReportFile(path.substring(0, path.lastIndexOf('/') + 1) + "/logs/record.log");
        } else {
          path = "./sserver.toml";
          //BrowseRecord.Instance().setReportFile("./logs/record.log");
        }
        File file1=new File(path);
        if(!file1.exists()){
          throw new FileNotFoundException("cannot found any sserver.toml");
        }
      }
      return Parameters.Initialize(path);
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return false;
  }

  public static void main(@NotNull String[] args) throws IOException, SVNException, InterruptedException {
    try {
      log.info("Git as svn version: {}", VersionInfo.getVersionInfo());
      if (!ParametersInitialize()) {
        log.error("load sserver.toml failed");
        return;
      }
      if (!Resque.RedissonInitialize(Parameters.Get().getRedisServer())) {
        log.info("Initialize redis client: {}", Parameters.Get().getRedisServer());
        return;
      }
      final SvnServer server = new SvnServer();
      server.start();
      Runtime.getRuntime().addShutdownHook(new Thread(() -> {
        try {
          server.shutdown(Parameters.Get().getShutdownTimeout());
        } catch (IOException | InterruptedException e) {
          log.error("Can't shutdown correctly", e);
        }
      }));
      server.join();
    } catch (Throwable e) {
      log.error("Fatal error: " + e.getMessage(), e);
      throw e;
    }
  }

  public static class CmdArgs {
    @Parameter(names = {"-c", "--config"}, description = "Configuration file name", required = true)
    @NotNull
    private File configuration;

    @Parameter(names = {"-s", "--show-config"}, description = "Show actual configuration on start")
    private boolean showConfig = false;

    @Parameter(names = {"--unsafe"}, description = "Allow unsafe developer-only configuration options")
    private boolean unsafeConfig = false;

    @Parameter(names = {"-h", "--help"}, description = "Show help", help = true)
    private boolean help = false;
  }

}
