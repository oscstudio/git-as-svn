package svnserver.oschina;

import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNErrorCode;
import org.tmatesoft.svn.core.SVNErrorMessage;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import svnserver.config.GitPusherConfig;
import svnserver.config.LocalConfig;
import svnserver.context.LocalContext;
import svnserver.context.SharedContext;
import svnserver.repository.RepositoryInfo;
import svnserver.repository.VcsAccess;
import svnserver.repository.VcsRepository;
import svnserver.repository.VcsRepositoryMapping;
import svnserver.repository.git.GitCreateMode;
import svnserver.repository.git.GitLocation;
import svnserver.repository.git.GitRepository;
import svnserver.repository.git.push.GitPushEmbeddedConfig;
import svnserver.repository.locks.PersistentLockFactory;
import svnserver.repository.mapping.RepositoryListMapping;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by charlie on 16-12-26.
 */
public class SubversionMapping implements VcsRepositoryMapping {
  @NotNull
  private static final Logger log = LoggerFactory.getLogger(SubversionMapping.class);
  @NotNull
  private final NavigableMap<String, GitRepository> mapping = new ConcurrentSkipListMap<>();
  @NotNull
  private final SharedContext context;
  @NotNull
  private GitPusherConfig pusher = GitPushEmbeddedConfig.instance;
  @NotNull
  private GitCreateMode createMode = GitCreateMode.ERROR;
  @NotNull
  private final SubversionMappingConfig config;
  @NotNull
  private List<LocalConfig> extensions = new ArrayList<>();

  public SubversionMapping(@NotNull SharedContext context, @NotNull SubversionMappingConfig config) {
    this.context = context;
    this.config = config;
  }

  @NotNull
  public SharedContext getContext() {
    return context;
  }

  @Nullable
  @Override
  public RepositoryInfo getRepository(@NotNull SVNURL url) throws SVNException {
    final Map.Entry<String, GitRepository> entry = RepositoryListMapping.getMapped(mapping, url.getPath());
    if (entry != null) {
      return new RepositoryInfo(SVNURL.create(url.getProtocol(), url.getUserInfo(), url.getHost(),
          url.getPort() == SVNURL.getDefaultPortNumber(url.getProtocol()) ? -1 : url.getPort(), entry.getKey(), true),
          entry.getValue());
    }
    return null;
  }

  @Nullable
  @Override
  public RepositoryInfo findRepository(@NotNull SVNURL baseURL, @NotNull SubversionPath path) throws SVNException {
    if (mapping.size() >= config.getCacheMaximumSize()) {
      /// TO remove first entry
      log.debug("current repository size {}, poll first entry", mapping.size());
      mapping.pollFirstEntry();
    }
    if (!path.RepositoryIsExists())
      return null;
    final String projectKey = path.getRepositoryPath() + "@" + path.getRefs();
    final Map.Entry<String, GitRepository> entry = RepositoryListMapping.getMapped(mapping, projectKey);
    if (entry != null) {
      log.debug("find project in cache {}", projectKey);
      if (entry.getValue().getUuid().equals(SubversionUuid.DiscoverSubversionUuidSimple(path.getRepositoryPath())))
        return new RepositoryInfo(baseURL, entry.getValue());
      removeRepository(path);
    }
    try {
      final VcsRepository repo = addRepository(path);
      if (repo == null)
        return null;
      return new RepositoryInfo(baseURL, repo);
    } catch (NullPointerException e) {
      if (SubversionUuid.DiscoverSubversionUuidSimple(path.getRepositoryPath()).equals(SubversionUuid.ZeroUUID))
        throw new SVNException(SVNErrorMessage.create(SVNErrorCode.RA_NO_REPOS_UUID, "Repository UUID is invalid"));
      log.error("NullPointerException", e);
    } catch (IOException e) {
      log.error("cannot addRepository ");
    }
    return null;
  }

  @Override
  public void initRevisions() throws IOException, SVNException {
    for (Map.Entry<String, GitRepository> entry : mapping.entrySet()) {
      entry.getValue().updateRevisions();
    }
  }

  @NotNull
  public Repository createRepository(@NotNull File fullPath, @NotNull String branch) throws IOException {
    //context.add(GitLocation.class, new GitLocation(fullPath));
    if (!fullPath.exists()) {
      log.info("Repository fullPath: {} - not exists, create mode: {}", fullPath, createMode);
      return createMode.createRepository(fullPath, branch);
    }
    log.info("Repository fullPath: {}", fullPath);
    return new FileRepository(fullPath);
  }

  @Nullable
  private GitRepository addRepository(@NotNull SubversionPath path) throws IOException, SVNException {
    File fullPath = new File(path.getRepositoryPath());
    if (!fullPath.exists())
      return null;
    final String projectKey = path.getRepositoryPath() + "@" + path.getRefs();
    final LocalContext local = new LocalContext(context, projectKey);
    local.add(GitLocation.class, new GitLocation(fullPath));
    local.add(VcsAccess.class, new SubversionAccess(local, config, path));
    GitRepository repository = new GitRepository(local, createRepository(fullPath, path.getRefs()),
        this.pusher.create(local), path.getRefs(), false, new PersistentLockFactory(local));
    for (LocalConfig extension : extensions) {
      extension.create(local);
    }
    if (mapping.compute(projectKey,
        (key, value) -> value != null && value.getRepository() == repository.getRepository() ? value
            : repository) == repository) {
      return repository;
    }
    return null;
  }

  private void closeRepository(@NotNull GitRepository repo) {
    try {
      repo.close();
    } catch (Exception e) {
      log.error("cannot close repository");
    }
  }

  @Override
  public void removeRepository(@NotNull SubversionPath path) {
    final String projectKey = path.getRepositoryPath() + "@" + path.getRefs();
    final GitRepository repo = mapping.get(projectKey);
    if (repo != null) {
      if (mapping.remove(projectKey, repo)) {
        closeRepository(repo);
      }
    }
  }
}
