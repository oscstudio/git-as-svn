/**
 * This file is part of git-as-svn. It is subject to the license terms
 * in the LICENSE file found in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/gpl-2.0.html. No part of git-as-svn,
 * including this file, may be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file.
 */
package svnserver.server;

import org.eclipse.jgit.errors.MissingObjectException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNErrorCode;
import org.tmatesoft.svn.core.SVNErrorMessage;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import svnserver.auth.*;
import svnserver.auth.Authenticator;
import svnserver.config.CacheConfig;
import svnserver.config.Config;
import svnserver.config.PersistentCacheConfig;
import svnserver.config.SharedConfig;
import svnserver.context.SharedContext;
import svnserver.ext.gitlab.auth.GitLabUserDB;
import svnserver.oschina.Parameters;
import svnserver.oschina.SubversionMappingConfig;
import svnserver.oschina.SubversionPath;
import svnserver.oschina.SubversionSession;
import svnserver.parser.MessageParser;
import svnserver.parser.SvnServerParser;
import svnserver.parser.SvnServerToken;
import svnserver.parser.SvnServerWriter;
import svnserver.parser.token.ListBeginToken;
import svnserver.parser.token.ListEndToken;
import svnserver.repository.RepositoryInfo;
import svnserver.repository.VcsAccess;
import svnserver.repository.VcsRepository;
import svnserver.repository.VcsRepositoryMapping;
import svnserver.server.command.*;
import svnserver.server.msg.AuthReq;
import svnserver.server.msg.ClientInfo;
import svnserver.server.step.Step;

import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Сервер для предоставления доступа к git-у через протокол subversion.
 *
 * @author Artem V. Navrotskiy <bozaro@users.noreply.github.com>
 */
public class SvnServer extends Thread {
  @NotNull
  private static final Logger log = LoggerFactory.getLogger(SvnServer.class);
  private static final long FORCE_SHUTDOWN = TimeUnit.SECONDS.toMillis(5);
  @NotNull
  private final Map<String, BaseCmd<?>> commands = new HashMap<>();
  @NotNull
  private final Map<Long, Socket> connections = new ConcurrentHashMap<>();
  @NotNull
  private final Map<Long,Long> sessionMap=new ConcurrentHashMap<>();
  @NotNull
  private final VcsRepositoryMapping repositoryMapping;
//  @NotNull
//  private final Config config;
  @NotNull
  private CacheConfig cacheConfig = new PersistentCacheConfig();
  @NotNull
  private final ServerSocket serverSocket;
  @NotNull
  private List<SharedConfig> shared = new ArrayList<>();
  @NotNull
  private final ExecutorService poolExecutor;
  @NotNull
  private final AtomicBoolean stopped = new AtomicBoolean(false);
  @NotNull
  private final AtomicLong lastSessionId = new AtomicLong();
  @NotNull
  private final AtomicLong scheduledTimes=new AtomicLong();
  @NotNull
  private final SharedContext context;
  @NotNull
  public List<SharedConfig> getShared() {
    return shared;
  }
  @NotNull
  public CacheConfig getCacheConfig() {
    return cacheConfig;
  }
  public SvnServer() throws IOException, SVNException {
    setDaemon(true);
    File file=new File(Parameters.Get().getBasePath().getAbsolutePath()+"/caches");
    if(!file.exists()){
      if(!file.mkdir()){
        log.error("create dir: {} failed ",file.getAbsolutePath());
        System.exit(-1);
      }
    }

    context = SharedContext.create(file, Parameters.createCache(file), this.getShared());
    //context.add(UserDB.class, GitLabUserDB());

    commands.put("commit", new CommitCmd());
    commands.put("diff", new DeltaCmd(DiffParams.class));
    commands.put("get-locations", new GetLocationsCmd());
    commands.put("get-location-segments", new GetLocationSegmentsCmd());
    commands.put("get-latest-rev", new GetLatestRevCmd());
    commands.put("get-dated-rev", new GetDatedRevCmd());
    commands.put("get-dir", new GetDirCmd());
    commands.put("get-file", new GetFileCmd());
    commands.put("get-iprops", new GetIPropsCmd());
    commands.put("log", new LogCmd());
    commands.put("reparent", new ReparentCmd());
    commands.put("check-path", new CheckPathCmd());
    commands.put("replay", new ReplayCmd());
    commands.put("replay-range", new ReplayRangeCmd());
    commands.put("rev-prop", new RevPropCmd());
    commands.put("rev-proplist", new RevPropListCmd());
    commands.put("stat", new StatCmd());
    commands.put("status", new DeltaCmd(StatusParams.class));
    commands.put("switch", new DeltaCmd(SwitchParams.class));
    commands.put("update", new DeltaCmd(UpdateParams.class));

    commands.put("lock", new LockCmd());
    commands.put("lock-many", new LockManyCmd());
    commands.put("unlock", new UnlockCmd());
    commands.put("unlock-many", new UnlockManyCmd());
    commands.put("get-lock", new GetLockCmd());
    commands.put("get-locks", new GetLocksCmd());

    //repositoryMapping = config.getRepositoryMapping().create(context);
    repositoryMapping= new SubversionMappingConfig().create(context);
    context.add(VcsRepositoryMapping.class, repositoryMapping);
    repositoryMapping.initRevisions();

    serverSocket = new ServerSocket();
    serverSocket.setReuseAddress(Parameters.Get().IsReuseAddress());
    serverSocket.bind(new InetSocketAddress(InetAddress.getByName(Parameters.Get().getAddress()),Parameters.Get().getPort()));
    if(Parameters.Get().IsUseCachedThreadPool()){
        poolExecutor = Executors.newCachedThreadPool();
    }else{
        poolExecutor = Executors.newFixedThreadPool(Parameters.Get().getThreads());
    }

    log.info("Server bind: {}", serverSocket.getLocalSocketAddress());
    context.ready();
  }

  public int getPort() {
    return serverSocket.getLocalPort();
  }

  @NotNull
  public SharedContext getContext() {
    return context;
  }

  @Override
  public void run() {
    log.info("Server is ready on port: {}", serverSocket.getLocalPort());
    //// to add scheduled, check session life
    ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
    log.info("Create Scheduled to checking session life cycle time");
    scheduledThreadPool.scheduleAtFixedRate(new Runnable() {
      @Override
      public void run() {
        final long times=scheduledTimes.incrementAndGet();
        final long totalSessions=lastSessionId.get();
        /// 5 *4 20min
        if(times % 4==0){
          log.info("Total number of historical sessions: {}",totalSessions);
          log.info("Current session count: {}", sessionMap.size());
        }
        sessionMap.forEach((id, beginTime) -> {
          if (System.currentTimeMillis() - beginTime > Parameters.Get().getTimeout() * 1000) {
            log.warn("[TIMEOUT] SessionId: {},Start Time: {}", id, beginTime);
            try {
              connections.get(id).close();
              sessionMap.remove(id);
            } catch (Exception e) {
              log.warn("Subversion Server Close Failed Session, Id:{}", id,e);
            }
          }
        });
      }
    }, 0, 5, TimeUnit.MINUTES);
    while (!stopped.get()) {
      final Socket client;
      try {
        client = this.serverSocket.accept();
      } catch (IOException e) {
        if (stopped.get()) {
          log.info("Server Stopped");
          break;
        }
        log.error("Error accepting client connection", e);
        continue;
      }
      long sessionId = lastSessionId.incrementAndGet();
      poolExecutor.execute(() -> {
        log.info("New connection from: {}", client.getRemoteSocketAddress());
        try (Socket clientSocket = client) {
          connections.put(sessionId, client);
          sessionMap.put(sessionId,System.currentTimeMillis());
          serveClient(clientSocket);
        } catch (EOFException | SocketException ignore) {
          // client disconnect is not a error
        } catch (SVNException | IOException e) {
          log.info("Client error:", e);
        } finally {
          connections.remove(sessionId);
          sessionMap.remove(sessionId);
          log.info("Connection from {} closed", client.getRemoteSocketAddress());
        }
      });
    }
  }

  public void serveClient(@NotNull Socket socket) throws IOException, SVNException {
    socket.setTcpNoDelay(true);
    final SvnServerWriter writer = new SvnServerWriter(new BufferedOutputStream(socket.getOutputStream()));
    final SvnServerParser parser = new SvnServerParser(socket.getInputStream());

    final ClientInfo clientInfo = exchangeCapabilities(parser, writer);
    SubversionPath subversionPath = SubversionPath.Builder(clientInfo.getUrl());
    if(subversionPath==null){
      final String msg=String.format("Wrong URL: %s, Can't find repository info, HelpURL: http://git.mydoc.io/?t=85189",clientInfo.getUrl().toString());
      BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.BAD_URL,msg));
      parser.skipItems();
      return;
    }


    final SVNURL baseURL=SVNURL.parseURIEncoded(subversionPath.getBaseURL());

    final SubversionSession session=Authorize(parser,writer,subversionPath);
    switch (session.getLastError()) {
      case SubversionSession.SUCCESS:
        if (!session.isReadAllow()) {
          BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.AUTHZ_UNREADABLE, "Not Found Repository"));
          log.info("User {} Access Private Repository: {}", session.getUser().getEmail(), subversionPath.getBaseURL());
          parser.skipItems();
        }
        break;
      case SubversionSession.REPOSITORY_NOT_EXISTS:
        BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.RA_SVN_REPOS_NOT_FOUND, session.getErrorMessage()));
        parser.skipItems();
        break;
      case SubversionSession.REPOSITORY_NOT_ALLOW_SVN:
        BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.RA_SVN_REPOS_NOT_FOUND, session.getErrorMessage()));
        parser.skipItems();
        break;
      case SubversionSession.AUTHORIZATION_FAILED:
        BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.RA_NOT_AUTHORIZED, session.getErrorMessage()));
        parser.skipItems();
        return;
      case SubversionSession.BRANCH_NOT_EXISTS:
        BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.BAD_URL, session.getErrorMessage()));
        parser.skipItems();
        break;
      default:
        BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.UNKNOWN, session.getErrorMessage()));
        parser.skipItems();
        break;
    }
    /// to update refs when current ref == HEAD
    subversionPath.updateCurrentRefs(session.getDefaultBranch());
    /// subversion check
    //final RepositoryInfo repositoryInfo = repositoryMapping.getRepository(clientInfo.getUrl());

    final RepositoryInfo repositoryInfo=repositoryMapping.findRepository(baseURL,subversionPath);

    if (repositoryInfo == null) {
      BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.RA_SVN_REPOS_NOT_FOUND, "Repository not found: " + clientInfo.getUrl()));
      return;
    }
    final SessionContext context = new SessionContext(parser, writer, this, repositoryInfo, clientInfo);
    //context.authenticate(hasAnonymousAuthenticator(repositoryInfo));
    context.setUser(session.getUser());
    final VcsRepository repository = context.getRepository();
    try{
      repository.updateRevisions();
    }catch(ClassCastException|MissingObjectException e){
      /// remove
      log.error("Require rebuild cache",e);
      repositoryMapping.removeRepository(subversionPath);
      BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.IO_ERROR, "Require rebuild cache: "+e.getMessage()));
      parser.skipItems();
    }catch(SVNException e){
      log.error("[SVNException] URL {} Exception {}",subversionPath.getBaseURL(),e.getErrorMessage().getMessage());
      BaseCmd.sendError(writer, e.getErrorMessage());
      parser.skipItems();
      return;
	}
	catch(NullPointerException e){
      repositoryMapping.removeRepository(subversionPath);
      log.error("NullPointerException",e);
      BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.IO_ERROR, "Please retry, cache will rebuild"));
      parser.skipItems();
      return;
  }
	catch(Exception e){
	    repositoryMapping.removeRepository(subversionPath);
      BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.IO_ERROR, "updateRevisions failed: "+e.getMessage()));
      parser.skipItems();
      return;
    }

    sendAnnounce(writer, repositoryInfo);

    while (!isInterrupted()) {
      try {
        Step step = context.poll();
        if (step != null) {
          step.process(context);
          continue;
        }

        final SvnServerToken token = parser.readToken();
        if (token != ListBeginToken.instance) {
          throw new IOException("Unexpected token: " + token);
        }
        final String cmd = parser.readText();
        BaseCmd command = commands.get(cmd);
        if (command != null) {
          log.debug("Receive command: {}", cmd);
          //// filter write command
          if(command.IsWriteCommand() && !session.isWriterAllow()){
            BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.AUTHZ_UNWRITABLE, "You're not authorized to write this repository"));
            parser.skipItems();
            return;
          }
          Object param = MessageParser.parse(command.getArguments(), parser);
          parser.readToken(ListEndToken.class);
          //noinspection unchecked
          command.process(context, param);
        } else {
          log.warn("Unsupported command: {}", cmd);
          BaseCmd.sendError(writer, SVNErrorMessage.create(SVNErrorCode.RA_SVN_UNKNOWN_CMD, "Unsupported command: " + cmd));
          parser.skipItems();
        }
      } catch (SVNException e) {
        if (e.getErrorMessage().getErrorCode() == SVNErrorCode.RA_NOT_AUTHORIZED) {
          log.warn("Command execution error: {}", e.getMessage());
        } else {
          log.error("Command execution error", e);
        }
        BaseCmd.sendError(writer, e.getErrorMessage());
      }
    }
  }

  private ClientInfo exchangeCapabilities(SvnServerParser parser, SvnServerWriter writer) throws IOException, SVNException {
    // Анонсируем поддерживаемые функции.
    writer
        .listBegin()
        .word("success")
        .listBegin()
        .number(2)
        .number(2)
        .listBegin()
        .listEnd()
        .listBegin();
    // Begin capabilities block.
    writer
        .word("edit-pipeline")         // This is required.
        .word("absent-entries")        // We support absent-dir and absent-dir editor commands
        //.word("commit-revprops") // We don't currently have _any_ revprop support
        //.word("mergeinfo")       // Nope, not yet
        .word("depth")
        .word("inherited-props")       // Need for .gitattributes and .gitignore
        .word("log-revprops");         // svn log --with-all-revprops
    if (Parameters.Get().IsCompressionEnabled()) {
      writer.word("svndiff1");         // We support svndiff1 (compression)
    }
    // End capabilities block.
    writer
        .listEnd()
        .listEnd()
        .listEnd();

    // Читаем информацию о клиенте.
    final ClientInfo clientInfo = MessageParser.parse(ClientInfo.class, parser);
    if (clientInfo.getProtocolVersion() != 2) {
      throw new SVNException(SVNErrorMessage.create(SVNErrorCode.VERSION_MISMATCH, "Unsupported protocol version: " + clientInfo.getProtocolVersion() + " (expected: 2)"));
    }
    return clientInfo;
  }
  @NotNull
  public SubversionSession Authorize(
          @NotNull SvnServerParser parser,
          @NotNull SvnServerWriter writer,
          @NotNull SubversionPath path)throws IOException, SVNException{
      writer
              .listBegin()
              .word("success")
              .listBegin()
              .listBegin()
              ///// now use PLAIN TEXT
              .word(String.join(" ", "PLAIN"))
              .listEnd()
              .string(Parameters.Get().getDomain())
              .listEnd()
              .listEnd();

      while (true) {
          // Читаем выбранный вариант авторизации.
          final AuthReq authReq = MessageParser.parse(AuthReq.class, parser);
          if (!authReq.getMech().equals("PLAIN")) {
              sendError(writer, "unknown auth type: " + authReq.getMech());
              continue;
          }
          final SubversionSession session=SubversionSession.SubversionAuthorize(parser,writer,authReq.getToken(),path);
          /// When session is null and authorization failed
          if (session == null||session.getLastError()==SubversionSession.AUTHORIZATION_FAILED) {
              sendError(writer, "incorrect credentials");
              continue;
          }

          writer
                  .listBegin()
                  .word("success")
                  .listBegin()
                  .listEnd()
                  .listEnd();

          //log.info("Session: {}", session.getRepositoryPath());
          return session;
      }
  }

  private boolean hasAnonymousAuthenticator(RepositoryInfo repositoryInfo) throws IOException {
    try {
      repositoryInfo.getRepository().getContext().sure(VcsAccess.class).checkRead(User.getAnonymous(), null);
      return true;
    } catch (SVNException e) {
      return false;
    }
  }

  private void sendAnnounce(@NotNull SvnServerWriter writer, @NotNull RepositoryInfo repositoryInfo) throws IOException {
    writer
        .listBegin()
        .word("success")
        .listBegin()
        .string(repositoryInfo.getRepository().getUuid())
        .string(repositoryInfo.getBaseUrl().toString())
        .listBegin()
            //.word("mergeinfo")
        .listEnd()
        .listEnd()
        .listEnd();
  }

  private static void sendError(SvnServerWriter writer, String msg) throws IOException {
    writer
        .listBegin()
        .word("failure")
        .listBegin()
        .string(msg)
        .listEnd()
        .listEnd();
  }

  public void startShutdown() throws IOException {
    if (stopped.compareAndSet(false, true)) {
      log.info("Shutdown server");
      serverSocket.close();
      poolExecutor.shutdown();
    }
  }

  public void shutdown(long millis) throws InterruptedException, IOException {
    startShutdown();
    if (!poolExecutor.awaitTermination(millis, TimeUnit.MILLISECONDS)) {
      forceShutdown();
    }
    join(millis);
    context.close();
    log.info("Server shutdowned");
  }

  private void forceShutdown() throws IOException, InterruptedException {
    for (Socket socket : connections.values()) {
      socket.close();
    }
    poolExecutor.awaitTermination(FORCE_SHUTDOWN, TimeUnit.MILLISECONDS);
  }

  public boolean isCompressionEnabled() {
    return Parameters.Get().IsCompressionEnabled();
  }
}
