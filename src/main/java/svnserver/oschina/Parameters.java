package svnserver.oschina;

import com.moandjiezana.toml.Toml;
import org.jetbrains.annotations.NotNull;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import svnserver.server.SvnServer;

import java.io.File;

/**
 * Created by charlie on 16-10-24.
 */
public class Parameters {
  //
  static private Parameters instance_ = null;
  public static final long MBSIZE = 1024 * 1024;
  private static final Logger log = LoggerFactory.getLogger(SvnServer.class);

  public static boolean Initialize(@NotNull String file) {
    try {
      File f = new File(file);
      Toml toml = new Toml().read(f);
      instance_ = new Parameters();
      instance_.shutdownTimeout = toml.getLong("Service.ShutdownTimeout", (long) 10);
      instance_.maximumCache = toml.getLong("Service.MaximumCache", (long) 2000).intValue();
      instance_.limitedSize = toml.getDouble("Service.LimitedSize", 400.0);
      instance_.urlPrefix = toml.getString("Service.ApiPrefix", "http://git.oschina.net/api/v3/");
      instance_.domain = toml.getString("Service.Domain", "git.oschina.net");
      instance_.timeout = toml.getLong("Service.Timeout", (long) 1800).intValue(); // 1800s
      instance_.fileLimitSize = toml.getLong("Service.FileLimitSize", (long) 100) * MBSIZE;
      //// default threads
      long MP = Runtime.getRuntime().availableProcessors();
      instance_.threads = toml.getLong("App.Threads", MP).intValue();
      instance_.port = toml.getLong("App.Port", (long) 3690).intValue();
      instance_.address = toml.getString("App.Address", "0.0.0.0");
      instance_.root = toml.getString("App.Root", "/home/git/repositories/");
      if (!instance_.root.endsWith("/"))
        instance_.root = instance_.root + "/";
      instance_.useCachedThreadPool = toml.getBoolean("App.UseCachedThreadPool", false);
      instance_.enableSync = toml.getBoolean("App.EnableSync", false);
      instance_.filterDomain = toml.getBoolean("App.FilterDomain", false);
      instance_.reuseAddress = toml.getBoolean("App.ReuseAddress", true);
      instance_.isCompressionEnabled = toml.getBoolean("App.CompressionEnabled", false);
      instance_.redisServer = toml.getString("App.RedisServer", "127.0.0.1:6379");
      instance_.basePath = f.getAbsoluteFile().getParentFile();
    } catch (Exception e) {
      log.error("Exception", e);
    }

    return true;
  }

  @NotNull
  public static Parameters Get() {
    return instance_;
  }

  public long getShutdownTimeout() {
    return shutdownTimeout;
  }

  public double getLimitedSize() {
    return limitedSize;
  }

  public String getUrlPrefix() {
    return urlPrefix;
  }

  public int getThreads() {
    return threads;
  }

  public String getDomain() {
    return domain;
  }

  public String getRoot() {
    return root;
  }

  public String getRedisServer() {
    return redisServer;
  }

  public int getPort() {
    return port;
  }

  public String getAddress() {
    return address;
  }

  public int getTimeout() {
    return timeout;
  }

  public long getFileLimitSize() {
    return fileLimitSize;
  }

  public int getMaximumCache() {
    return maximumCache;
  }

  public boolean IsEnableSync() {
    return enableSync;
  }

  public boolean IsUseCachedThreadPool() {
    return useCachedThreadPool;
  }

  public boolean IsFilterDomain() {
    return filterDomain;
  }

  public boolean IsReuseAddress() {
    return reuseAddress;
  }

  public boolean IsCompressionEnabled() {
    return isCompressionEnabled;
  }

  @NotNull
  public File getBasePath() {
    return basePath;
  }

  ///
  private double limitedSize;
  private String urlPrefix;
  private String domain;
  private String address;
  private String root;
  private String redisServer;
  private int port;
  private int threads;
  private int timeout;
  private int maximumCache;
  private long fileLimitSize;
  private long shutdownTimeout;
  private File basePath;
  private boolean enableSync;
  private boolean useCachedThreadPool;
  private boolean filterDomain;
  private boolean reuseAddress;
  private boolean isCompressionEnabled;

  @NotNull
  public static File joinPath(@NotNull File basePath, @NotNull String localPath) {
    final File path = new File(localPath);
    return path.isAbsolute() ? path : new File(basePath, localPath);
  }

  @NotNull
  static public DB createCache(@NotNull File basePath) {
    return DBMaker.newFileDB(joinPath(basePath, "svnserver.mapdb")).closeOnJvmShutdown().asyncWriteEnable()
        .mmapFileEnable().cacheSoftRefEnable().make();
  }
}