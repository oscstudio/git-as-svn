package svnserver.oschina;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.redisson.Redisson;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;

/**
 * Created by charlie on 16-12-7.
 * Redis  que
 */
public class Resque {
  private static RedissonClient redisson = null;
  private static RList<String> rList = null;

  public static RedissonClient getRedisson() {
    return redisson;
  }

  public static boolean RedissonInitialize(@NotNull String address) {
    Config config = new Config();
    config.setCodec(StringCodec.INSTANCE);
    config.useSingleServer().setAddress(address);
    redisson = Redisson.create(config);
    if (redisson == null)
      return false;
    rList = redisson.getList("resque:gitlab:queue:post_receive");
    return (rList != null);
  }

  private static String getRepoPathWithNameSpace(@NotNull String repo) {
    //String repo_path=repo.substring()
    ///  /home/git/repositories/sv/svnserver/newos.git
    if (repo.endsWith("/"))
      repo = repo.substring(0, repo.length() - 1);
    int start = repo.lastIndexOf("/", repo.lastIndexOf("/") - 1) + 1;
    int end = repo.lastIndexOf(".git");
    return repo.substring(start, end);
  }

  public static boolean ResqueInsert(@NotNull String gitdir, @NotNull String refname, @Nullable String oldrev,
      @NotNull String newrev, String userid) {
    String oldrev2 = oldrev == null ? "0000000000000000000000000000000000000000" : oldrev;
    String m = String.format("{\"class\":\"PostReceive\",\"args\":[\"%s\",\"%s\",\"%s\",\"%s\",\"user-%s\"]}",
        getRepoPathWithNameSpace(gitdir), oldrev2, newrev, refname, userid);
    return rList.add(m);
  }
}