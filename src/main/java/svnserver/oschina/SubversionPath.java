/*
 * File: SVNPathInfo.java
 * Author: Force Charlie
 * CreateTime: @2015.10
 * Copyright (C) 2017. OSChina.NET.All Rights Reserved.
 */

package svnserver.oschina;

import org.jetbrains.annotations.NotNull;
import org.tmatesoft.svn.core.SVNURL;

import java.io.File;
import java.util.StringTokenizer;


public class SubversionPath {
    private String repositoryPath;
    private ReferenceIndex refsIndex;
    private String owner;
    private String name;
    private String refs;
    private String prefix;
    private String baseURL;
    public String getRepositoryPath() {
        return repositoryPath;
    }
    public ReferenceIndex getRefsIndex() {
        return refsIndex;
    }
    @NotNull
    public String getOwner() {
        return owner;
    }
    @NotNull
    public String getName() {
        return name;
    }
    @NotNull
    public String getRefs() {
        return refs;
    }
    public void updateCurrentRefs(@NotNull String refname){
        if(refs.equals("HEAD")){
            this.refs=refname;
        }
    }
    @NotNull
    public String getPrefix() {
        return prefix;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public boolean RepositoryIsExists(){
        File f=new File(repositoryPath);
        return (f.exists() &&f.isDirectory());
    }

    public static SubversionPath Builder(@NotNull SVNURL svnurl){
        String path = svnurl.getPath();
        SubversionPath subversionPath = new SubversionPath();
        StringTokenizer stringTokenizer = new StringTokenizer(path, "/");
        if (stringTokenizer.countTokens() < 2) return null;
        /**
         *  Owner and Repository name get
         * */
        subversionPath.owner=stringTokenizer.nextToken();
        subversionPath.name=stringTokenizer.nextToken();

        StringBuilder baseURL = new StringBuilder();
        baseURL.append("svn://");
        if(svnurl.getUserInfo()!=null){
            baseURL.append(svnurl.getUserInfo());
            baseURL.append("@");
        }
        ////
        if(Parameters.Get().IsFilterDomain()){
            baseURL.append(Parameters.Get().getDomain());
        }else{
            baseURL.append(svnurl.getHost());
        }
        if(svnurl.hasPort()){
            baseURL.append(":");
            baseURL.append(svnurl.getPort());
        }
        baseURL.append("/");
        baseURL.append(subversionPath.getOwner());
        baseURL.append("/");
        baseURL.append(subversionPath.getName());

        subversionPath.repositoryPath=Parameters.Get().getRoot()+
                subversionPath.getOwner().substring(0,2)+"/"+
                subversionPath.getOwner()+"/"+
                subversionPath.getName()+".git";

        int counts = stringTokenizer.countTokens();
        if (counts == 0) {
            subversionPath.refsIndex=ReferenceIndex.ReferenceHEAD;
            subversionPath.refs="HEAD";
            subversionPath.prefix="/";

            subversionPath.baseURL=baseURL.toString();
            return subversionPath;
        }

        String rflags = stringTokenizer.nextToken();
        //////////////////////////////////////
        if (rflags == null) return null;
        switch (rflags) {
            case "trunk": {
                subversionPath.refsIndex=ReferenceIndex.ReferenceTrunk;
                subversionPath.refs="master";

                baseURL.append("/trunk");
                subversionPath.baseURL=baseURL.toString();
                if (stringTokenizer.countTokens() == 0) {
                    subversionPath.prefix="/";
                    return subversionPath;
                }
                StringBuilder prefix = new StringBuilder();
                while (stringTokenizer.countTokens() > 0) {
                    prefix.append("/");
                    prefix.append(stringTokenizer.nextToken());
                }
                subversionPath.prefix=prefix.toString();
            }
            break;
            case "branches": {
                if (stringTokenizer.countTokens() == 0) return null;
                subversionPath.refsIndex=ReferenceIndex.ReferenceBranch;
                String branches = stringTokenizer.nextToken();
                subversionPath.refs=branches;

                baseURL.append("/branches/");
                baseURL.append(branches);
                subversionPath.baseURL=baseURL.toString();

                if (stringTokenizer.countTokens() == 0) {
                    subversionPath.prefix="/";
                } else {
                    StringBuilder prefix = new StringBuilder();
                    while (stringTokenizer.countTokens() > 0) {
                        prefix.append("/");
                        prefix.append(stringTokenizer.nextToken());
                    }
                    subversionPath.prefix=prefix.toString();
                }

            }
            break;
            case "tags": {
                if (stringTokenizer.countTokens() == 0) return null;
                subversionPath.refsIndex=ReferenceIndex.ReferenceTags;
                String tags = stringTokenizer.nextToken();
                subversionPath.refs=tags;

                baseURL.append("/tags/");
                baseURL.append(tags);
                subversionPath.baseURL=baseURL.toString();
                //svnPathInfo.setBaseURL(baseURL);
                if (stringTokenizer.countTokens() == 0) {
                    subversionPath.prefix="/";
                } else {
                    StringBuilder prefix = new StringBuilder();
                    while (stringTokenizer.countTokens() > 0) {
                        prefix.append("/");
                        prefix.append(stringTokenizer.nextToken());
                    }
                    subversionPath.prefix=prefix.toString();
                }
            }
            break;
            default: {
                subversionPath.refsIndex=ReferenceIndex.ReferenceHEAD;
                subversionPath.refs="HEAD";
                subversionPath.baseURL=baseURL.toString();
                StringBuilder prefix = new StringBuilder();
                prefix.append("/");
                prefix.append(rflags);
                while (stringTokenizer.countTokens() > 0) {
                    prefix.append("/");
                    prefix.append(stringTokenizer.nextToken());
                }
                subversionPath.prefix=prefix.toString();
            }
            break;
        }
        return subversionPath;
    }
}