package svnserver.oschina;

import org.jetbrains.annotations.NotNull;
import com.moandjiezana.toml.Toml;

import java.io.File;

public class SubversionDB {
    private boolean blocked;
    public boolean isBlocked() {
        return blocked;
    }
    private double sizelimit;
    public double getSizeLimit(){
        return sizelimit;
    }
    private long limitFileSize;
    public long getFileLimitSize(){
        return limitFileSize;
    }
    public static SubversionDB Builder(@NotNull String gitdir){
        SubversionDB db=new SubversionDB();
        File f=new File(gitdir,"svn.toml");
        if(f.exists()&&!f.isDirectory()){
            Toml toml = new Toml().read(f);
            db.sizelimit= toml.getDouble("Repository.LimitSize",Parameters.Get().getLimitedSize());
            db.limitFileSize=toml.getLong("Repository.FileLimitSize",(long)100)* Parameters.MBSIZE;
            db.blocked=toml.getBoolean("Repository.Blocked",false);
        }else{
            db.sizelimit=Parameters.Get().getLimitedSize();
            db.limitFileSize=100*Parameters.MBSIZE;
            db.blocked=false;
        }
        return db;
    }
}
