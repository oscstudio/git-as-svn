package svnserver.oschina;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.UUID;

public class SubversionUuid {
  private static final Logger log = LoggerFactory.getLogger(SubversionUuid.class);
  @NotNull
  public static final String ZeroUUID = "00000000-0000-0000-0000-000000000000";
  private UUID uuid;

  public String getUuid() {
    if (uuid == null)
      return ZeroUUID;
    return uuid.toString();
  }

  /// load uuid from disk file
  private boolean InitializeLoadingUuid(@NotNull File file) {
    try {
      FileReader fr = new FileReader(file);
      BufferedReader br = new BufferedReader(fr);
      final String line = br.readLine();
      fr.close();
      uuid = UUID.fromString(line);
    } catch (IOException e) {
      log.error("InitializeLoading Uuid failed", e);
      return false;
    }
    return true;
  }

  /// save uuid to disk Immersion
  private boolean ImmobilizationUuid(@NotNull File file) {
    FileWriter fw;
    try {
      fw = new FileWriter(file);
      fw.write(this.getUuid() + "\n");
      fw.close();
    } catch (Exception e) {
      log.error("Exception", e);
      return false;
    }
    return true;
  }

  private static SubversionUuid DiscoverSubversionUuidInternal(@NotNull String ufile) {
    SubversionUuid su = new SubversionUuid();
    File file = new File(ufile);
    if (file.exists()) {
      if (file.isDirectory()) {
        log.error("Subversion UUID File is directory {}", ufile);
        return null;
      }
      if (!su.InitializeLoadingUuid(file)) {
        if (!file.delete()) {
          log.error("cannot delete file {}", ufile);
          return null;
        }
        su.uuid = UUID.randomUUID();
        su.ImmobilizationUuid(file);
      }
      //// to gen uuid
    } else {
      su.uuid = UUID.randomUUID();
      su.ImmobilizationUuid(file);
    }
    return su;
  }

  public static String DiscoverDefaultBranch(@NotNull String bareDir) {
    String ref = "master";
    final String headfile = bareDir + "/HEAD";
    File file = new File(headfile);
    if (!file.exists() || file.isDirectory())
      return ref;
    try {
      FileReader fr = new FileReader(file);
      BufferedReader br = new BufferedReader(fr);
      final String line = br.readLine();
      int N = line.indexOf("ref: refs/heads/");
      if (N >= 0)
        ref = line.substring(N + "ref:refs/heads/".length()+1);
      br.close();
    } catch (IOException e) {
      log.error("IOException:", e);
    }
    return ref;
  }

  public static SubversionUuid DiscoverSubversionUuid(@NotNull String bareDir) {
    final String ufile = bareDir + "/svn.uuid";
    return DiscoverSubversionUuidInternal(ufile);
  }

  public static SubversionUuid DiscoverSubversionUuidEx(@NotNull String bareDir, @NotNull String branch) {
    final String ufile = bareDir + "/svn." + branch + ".uuid";
    return DiscoverSubversionUuidInternal(ufile);
  }

  @NotNull
  public static String DiscoverSubversionUuidSimple(@NotNull String bareDir) {
    final String ufile = bareDir + "/svn.uuid";
    File file = new File(ufile);
    if (file.exists()) {
      if (file.isDirectory()) {
        log.error("Subversion UUID File is directory {}", ufile);
        return ZeroUUID;
      }
      try {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        final String line = br.readLine();
        fr.close();
        return UUID.fromString(line).toString();
      } catch (IOException e) {
        log.error("Discover UUID failed", e);
      }
    }
    return ZeroUUID;
  }
}