package svnserver.oschina;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

/**
 * Created by charlie on 17-2-23.
 * exe post-receive to sync
 */
public class SyncProcessBuilder {
  public static boolean Launcher(@NotNull File gitdir, String ExternalId){
    String file=gitdir.getAbsolutePath();
    if(file.endsWith("/")){
      file+="hooks/post-receive";
    }else{
      file+="hooks/post-receive";
    }
    final ProcessBuilder processBuilder=new ProcessBuilder(file).directory(gitdir);
    processBuilder.environment().put("GL_ID", "task-"+ExternalId);
    try{
      Process p=processBuilder.start();
      p.waitFor();
    }catch (IOException |InterruptedException e){
      return false;
    }
    return true;
  }
}
