package svnserver.oschina;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.tmatesoft.svn.core.SVNException;
import svnserver.auth.User;
import svnserver.context.LocalContext;
import svnserver.repository.VcsAccess;
import java.io.IOException;

/**
 * Created by charlie on 16-12-26.
 */
public class SubversionAccess implements VcsAccess {

  public SubversionAccess(@NotNull LocalContext local, @NotNull SubversionMappingConfig config, @NotNull SubversionPath path) {
  }

  @Override
  public void checkRead(@NotNull User user, @Nullable String path) throws SVNException, IOException {
    //// empty
  }

  @Override
  public void checkWrite(@NotNull User user, @Nullable String path) throws SVNException, IOException {
    //// empty

  }
}