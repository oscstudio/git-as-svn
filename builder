#!/bin/sh
basepath=$(cd `dirname $0`; pwd)
cd $basepath
./gradlew layout
if [ $? -ne 0 ]; then
	exit 2
else
	exit 0
fi
