package svnserver.oschina;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.internal.util.SVNBase64;
import svnserver.auth.User;
import svnserver.parser.SvnServerParser;
import svnserver.parser.SvnServerWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CharlieInc on 2016/12/25.
 */
public class SubversionSession {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SubversionSession.class);
    private static PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
    private final static int DEFAULT = 48;

    //final static int DEFAULT=Runtime.getRuntime().availableProcessors() *2;
    static {
        manager.setMaxTotal(DEFAULT);
        manager.setDefaultMaxPerRoute(DEFAULT);
    }

    private static String HTTPPostWarp(@NotNull String url, @NotNull List<NameValuePair> formParams) {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(manager).setUserAgent("SVN/2.0.1")
                .build();
        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(8000).setConnectTimeout(8000).build();
        httpPost.setConfig(requestConfig);
        UrlEncodedFormEntity uefEntity;
        HttpResponse httpResponse;
        try {
            uefEntity = new UrlEncodedFormEntity(formParams, "UTF-8");
            httpPost.setEntity(uefEntity);
            httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() >= 300)
                return null;
            return EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            log.error("URL {}", url);
            log.error("Error: ", e);
        }
        return null;
    }

    public static final int SUCCESS = 0;
    public static final int REPOSITORY_NOT_EXISTS = 1;
    public static final int REPOSITORY_NOT_ALLOW_SVN = 2;
    public static final int AUTHORIZATION_FAILED = 3;
    public static final int BRANCH_NOT_EXISTS = 4;
    public static final int INTERNAL_SERVER_ERROR = 5;
    public static final int INTERNAL_API_FAILED = 6;
    public static final int LARGE_SIZE_REPOSITORY = 7;
    public static final int OTHER_ERRORS = 8;
    private int lastError;
    private int userid;
    private boolean isPublic;
    private boolean writerAllow;
    private boolean readAllow;
    private long fileLimitSize;
    private String repositoryPath;
    private String errorMessage;
    private String defaultBranch;
    private String authorizeUsername;
    private String authorizeUserViewName;
    private User user;

    public int getLastError() {
        return lastError;
    }

    public String getRepositoryPath() {
        return repositoryPath;
    }

    public long getFileLimitSize() {
        return fileLimitSize;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getDefaultBranch() {
        return defaultBranch;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public boolean isWriterAllow() {
        return writerAllow;
    }

    public boolean isReadAllow() {
        return readAllow;
    }

    public String getAuthorizeUsername() {
        return authorizeUsername;
    }

    public String getAuthorizeUserViewName() {
        return authorizeUserViewName;
    }

    public User getUser() {
        return user;
    }

    private static SubversionSession SubversionAuthorizeInternal(@NotNull String email, @NotNull String password,
            @NotNull SubversionPath subversionPath) {
        SubversionSession ss = new SubversionSession();
        SubversionDB db = SubversionDB.Builder(subversionPath.getRepositoryPath());
        double sz;
        if (db != null) {
            sz = db.getSizeLimit();
            if (db.isBlocked()) {
                ss.lastError = SubversionSession.REPOSITORY_NOT_ALLOW_SVN;
                ss.errorMessage = "Repository is marked blocked, Please email to git@oschina.net";
                return ss;
            }
            ss.fileLimitSize = db.getFileLimitSize();
        } else {
            sz = Parameters.Get().getLimitedSize();
            ss.fileLimitSize = Parameters.Get().getFileLimitSize();
        }
        try {
            String owner = URLEncoder.encode(subversionPath.getOwner(), "UTF-8");
            String repo = URLEncoder.encode(subversionPath.getName(), "UTF-8");
            String xemail = URLEncoder.encode(email, "UTF-8");
            owner = owner.replaceAll("\\.", "+");
            repo = repo.replaceAll("\\.", "+");
            final String url = Parameters.Get().getUrlPrefix() + "internal/subversion/" + owner + "%2F" + repo + "/"
                    + xemail;
            List<NameValuePair> formParams = new ArrayList<>();
            formParams.add(new BasicNameValuePair("password", password));
            formParams.add(new BasicNameValuePair("branch", subversionPath.getRefs()));
            String jsonText = HTTPPostWarp(url, formParams);
            log.debug("JSON result:\n{}", jsonText);
            if (jsonText == null) {
                log.info("cannot access api interface");
                ss.lastError = SubversionSession.INTERNAL_SERVER_ERROR;
                ss.errorMessage = "Authorize server is not responding correctly ";
                return ss;
            }
            ////
            JsonParser jsonParser = new JsonParser();
            JsonObject obj = jsonParser.parse(jsonText).getAsJsonObject();

            if (!obj.has("error")) {
                ss.lastError = SubversionSession.INTERNAL_API_FAILED;
                ss.errorMessage = "Fatal: error json result,not found error code";
                return ss;
            }
            ss.lastError = obj.get("error").getAsInt();
            if (ss.lastError == 0 && obj.has("real_path")) {
                ss.defaultBranch = obj.has("default_branch") ? obj.get("default_branch").getAsString() : "master";
                ss.isPublic = obj.has("public") && obj.get("public").getAsBoolean();
                ss.repositoryPath = obj.get("real_path").getAsString();
                ss.writerAllow = obj.has("writer_allow") && obj.get("writer_allow").getAsBoolean();
                ss.readAllow = obj.has("read_allow") && obj.get("read_allow").getAsBoolean();
                ss.authorizeUsername = obj.has("auth_username") ? obj.get("auth_username").getAsString() : email;
                ss.authorizeUserViewName = obj.has("auth_name") ? obj.get("auth_name").getAsString()
                        : ss.authorizeUsername;
                final double projectSizeLimit = obj.has("project_limitsize")
                        ? (double) obj.get("project_limitsize").getAsInt()
                        : sz;
                if (obj.has("user_id")) {
                    ss.userid = obj.get("user_id").getAsInt();
                } else {
                    ss.userid = -1;
                }
                ss.user = User.create(ss.authorizeUsername, ss.authorizeUserViewName, email, String.valueOf(ss.userid));
                final long fileLimitSize = obj.has("file_limitsize")
                        ? obj.get("file_limitsize").getAsLong() * Parameters.MBSIZE
                        : ss.fileLimitSize;
                ss.user.setFileLimitSize(fileLimitSize);
                /// if json has size field, we check this repository storage size, > X MB, cannot use subversion checkout or commit.
                if (obj.has("size") && obj.get("size").getAsDouble() > projectSizeLimit) {
                    ss.lastError = SubversionSession.LARGE_SIZE_REPOSITORY;
                    ss.errorMessage = String.format(
                            "This Repository  size is %.2f MB, more than %.2f MB, HelpURL: http://git.mydoc.io/?t=85189",
                            obj.get("size").getAsDouble(), projectSizeLimit);
                } else {
                    ss.errorMessage = (obj.has("message") ? obj.get("message").getAsString() : "unknown error message");
                }
            } else {
                ss.errorMessage = (obj.has("message") ? obj.get("message").getAsString() : "unknown error message");
            }
            return ss;
        } catch (JsonSyntaxException e) {
            ss.errorMessage = "Json Syntax Exception";
            log.error("JsonSyntaxException", e);
        } catch (UnsupportedEncodingException e) {
            log.error("UnsupportedEncodingException", e);
            ss.errorMessage = "UnsupportedEncodingException";
        } catch (Exception e) {
            ss.errorMessage = e.getMessage();
        }
        ss.lastError = SubversionSession.OTHER_ERRORS;
        return ss;
    }

    public static SubversionSession SubversionAuthorize(@NotNull SvnServerParser parser,
            @NotNull SvnServerWriter writer, @NotNull String token, @NotNull SubversionPath subversionPath)
            throws IOException, SVNException {
        final String[] credentials = new String(fromBase64(token), StandardCharsets.US_ASCII).split("\u0000");
        if (credentials.length < 3)
            return null;
        final String username = credentials[1];
        final String password = credentials[2];
        return SubversionSession.SubversionAuthorizeInternal(username, password, subversionPath);
    }

    private static byte[] fromBase64(@Nullable String src) {
        if (src == null) {
            return new byte[0];
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        for (int i = 0; i < src.length(); i++) {
            char ch = src.charAt(i);
            if (!Character.isWhitespace(ch) && ch != '\n' && ch != '\r') {
                bos.write((byte) ch & 0xFF);
            }
        }
        byte[] cbytes = new byte[src.length()];
        src = new String(bos.toByteArray(), StandardCharsets.US_ASCII);
        int clength = SVNBase64.base64ToByteArray(new StringBuffer(src), cbytes);
        byte[] result = new byte[clength];
        // strip trailing -1s.
        for (int i = clength - 1; i >= 0; i--) {
            if (i == -1) {
                clength--;
            }
        }
        System.arraycopy(cbytes, 0, result, 0, clength);
        return result;
    }
}
