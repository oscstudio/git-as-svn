package svnserver.oschina;

import org.jetbrains.annotations.NotNull;
import org.tmatesoft.svn.core.SVNException;
import svnserver.config.GitRepositoryConfig;
import svnserver.config.RepositoryMappingConfig;
import svnserver.context.SharedContext;
import svnserver.repository.VcsRepositoryMapping;

import java.io.IOException;

/**
 * Created by charlie on 16-12-26.
 */
public class SubversionMappingConfig implements RepositoryMappingConfig {
  private GitRepositoryConfig template=new GitRepositoryConfig();
  private int cacheTimeSec = 15;
  private int cacheMaximumSize = 1000;
  private String path = "/home/git/repositories/";

  @NotNull
  public String getPath() {
    return path;
  }

  public int getCacheTimeSec() {
    return cacheTimeSec;
  }

  public int getCacheMaximumSize() {
    return cacheMaximumSize;
  }
  @NotNull
  public GitRepositoryConfig getTemplate() {
    return template;
  }
  @NotNull
  public VcsRepositoryMapping create(@NotNull SharedContext context) throws IOException, SVNException{
    path=Parameters.Get().getRoot();
    cacheMaximumSize=Parameters.Get().getMaximumCache();
    return new SubversionMapping(context,this);
  }
}
